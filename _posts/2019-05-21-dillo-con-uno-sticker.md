---
layout: post
title: Dillo con uno Sticker!
created: 1558445207
---
<p style="text-align: center">
    <img style="max-width: 100%" src="/assets/posts/images/stickers.jpg">
</p>
<p>
    Ogni anno, in occasione del <a href="http://www.linuxday.it/">Linux Day</a> di fine ottobre, <a href="/">Italian Linux Society</a> distribuisce a tutti gli organizzatori della manifestazione un pacco di spille e adesivi da distribuire al pubblico nel corso dell'iniziativa stessa.
</p>
<p>
    Oggi estendiamo questa opportunità a tutti coloro che svolgono altre attività di divulgazione nel corso dell'anno, come corsi e incontri, o più semplicemente vogliono contribuire a promuovere Linux nel proprio ufficio o nella propria scuola. Chi vuole ricevere, gratuitamente, una busta di adesivi con <a href="https://www.linux.it/">il pinguino di linux.it</a> e con <a href="https://www.linux.it/usa-analizza-modifica-condividi">il logo del software libero</a> può inviare una mail a webmaster@linux.it indicando l'indirizzo per la spedizione postale e l'occasione e/o il luogo dove si intendono distribuire. Nel giro di un paio di settimane riceverà il plico.
</p>
<p>
    Attenzione: non spediamo adesivi a titolo individuale, uno alla volta. Chi li chiede ne riceve almeno 20 per tipo e deve impegnarsi a distribuirli ad altri, prendendo parte attiva nella promozione di Linux!
</p>
<p>
    Approfittiamo dell'occasione per ricordare a chi ha (o conosce) un negozio di informatica che offre assistenza su Linux che è ancora possibile chiedere <a href="http://www.linuxsi.com/partecipa/">l'adesivo di LinuxSi</a>, da attaccare alla propria vetrina per segnalare la propria disponibilità professionale.
</p>
