---
layout: post
title: Cookie Law, a Modo Nostro
created: 1433541267
---
Da diversi giorni sui social network e nelle mailing list si sta discutendo della cosiddetta <a rel="nofollow" href="http://www.garanteprivacy.it/cookie">"Cookie Law"</a>, legge promossa dall'Autorità Garante per la Privacy e a sua volta implementazione di una direttiva europea entrata in vigore all'inizio di giugno. In breve, e senza scendere nel merito, la norma intende tutelare coloro che navigano sulla Rete dagli oramai onnipresenti e pervasivi strumenti di tracciamento e profilazione, i quali appunto fanno uso dei <a rel="nofollow" href="http://it.wikipedia.org/wiki/Cookie">cookies</a> - piccoli pacchetti di dati che accompagnano le richieste avanzate verso un sito web, comunemente usati sull'Internet e di per loro nient'affatto malevoli bensì estremamente utili - per identificare un utente nei suoi spostamenti tra una pagina e l'altra. Da pochi giorni, i siti che in modo più o meno diretto sono vettore di tali cookies di tracciamento - magari anche in buona fede, facendo uso di componenti HTML inclusi da altre piattaforme o di certi strumenti per l'analisi del traffico - devono rendere noto ai loro utenti il fatto, per mezzo di un banner che probabilmente avete già avuto modo di notare su diverse pagine.

Diverse pagine, ma non questa. Né tantomeno quelle degli altri siti del network ILS. Questo non per inadempienza, ma proprio perché qui non si fa uso di cookies potenzialmente dannosi per la privacy.

Abbiamo colto l'occasione dell'entrata in vigore della suddetta legge per revisionare le pagine di <a href="/">ils.org</a>, <a href="http://www.linux.it">linux.it</a>, <a href="http://lugmap.linux.it">lugmap.linux.it</a> e tutti gli altri, ed eliminare le già poche fonti di cookies estranei al funzionamento delle pagine stesse. Non è sempre stato semplice ed anzi per alcune situazioni occorrerà trovare una soluzione alternativa all'inclusione di taluni componenti HTML esterni, ma del resto l'impresa non è impossibile. E vi invitiamo caldamente a fare altrettanto sulle vostre pagine, ispezionando almeno una volta i contenuti inclusi da fonti terze.

In particolare, vi consigliamo di:
<ul>
<lI>includere solo dei link alle vostre pagine di rappresentanza sui social network (ad esempio l'<a rel="nofollow" href="https://twitter.com/italinuxsociety">account Twitter di Italian Linux Society</a> o la <a rel="nofollow" href="https://www.facebook.com/LinuxDayItalia">pagina Facebook del Linux Day</a>), anziché dei widgets complessi che, anche solo all'atto della visualizzazione, attivano funzioni Javascript sul browser degli utenti ed inviano dati inattesi</li>
<li>utilizzare <a rel="nofollow" href="http://piwik.org/">Piwik</a> come strumento di analisi del traffico, per continuare a raccogliere dati utili sulle pagine più visitate senza per questo far convergere su piattaforme terze dettagli potenzialmente sensibili sui vostri utenti</li>
</ul>

Qualora doveste individuare cookies inattesi sui siti del network ILS, vi preghiamo di segnalarlo tempestivamente all'indirizzo mail webmaster@linux.it affinché possano essere rimossi.
